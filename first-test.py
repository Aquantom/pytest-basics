import pytest

def add(a, b):
    return a + b

def mul(a, b):
    return a + b   # It should be mulitplication and not addition

# Writing our first test case in pytest
def test_add():
    assert add(2, 3) == 5

def test_add1():
    assert add(4, 7) == 11

def test_mul():
    assert mul(2, 3) == 6
   